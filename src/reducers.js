import { combineReducers } from "redux";
const initState = {
    goals: [
        {
            title: "Goal one",
            description: "First goal's description"
        },
        {
            title: "Goal two",
            description: "Second goal's description"
        }
    ]
};
const goalsReducer = (states = initState, action) => {
    switch (action.type) {
        // ajax action
        case "GET_GOALS":
            return { goals: action.goals };
        case "SAVE_GOALS":
            return { ...states };
        case "DELETE_GOAL":
            return deleteGoals(states, action);
        // local actions
        case "UPDATE_GOAL":
            return updateGoals(states, action);
        case "NEW_GOAL":
            return { ...states, goals: [...states.goals, action.goal] };
        default:
            break;
    }
    return states;
};

const deleteGoals = (states, action) => {
    const state = states.goals.filter((e, i) => i !== action.deleteId);
    return { ...states, goals: state };
};

const updateGoals = (states, action) => {
    const state = [...states.goals];
    state[action.goal.id] = action.goal;
    return { ...states, goals: state };
};

export default combineReducers({
    goals: goalsReducer
});
