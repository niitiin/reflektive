import * as service from "./service";
// local actions
export const updateGoal = (goal, dispatch) => {
    return dispatch({
        type: "UPDATE_GOAL",
        goal
    });
};

export const newGoal = (goal, dispatch) => {
    return dispatch({
        type: "NEW_GOAL",
        goal
    });
};

// ajax actions
export const getGoals = dispatch => {
    return service.getGoals().then(({ data }) => {
        dispatch({
            type: "GET_GOALS",
            goals: data.data
        });
    });
};
export const saveGoals = dispatch => {
    return service.saveGoals().then(({ data }) => {
        dispatch({
            type: "SAVE_GOALS",
            message: data.message
        });
    });
};
export const removeGoal = (id, dispatch) => {
    //  commented due to cors issue
    // return service.deleteGoals().then(({ data }) => {});
    dispatch({
        type: "DELETE_GOAL",
        deleteId: id
    });
};
