import React from "react";

export default function Navbar() {
    return (
        <nav className="navbar">
            <span className="navbar-text">Reflective UI Assignment</span>
        </nav>
    );
}
