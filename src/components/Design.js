import React, { Component } from "react";

import person from "../assets/person.jpeg";
import person2 from "../assets/person2.jpeg";
import arrow from "../assets/arrow.svg";

export default class Design extends Component {
    render() {
        return (
            <div className="card card-body">
                <div className="thread">
                    <div className="thread-title">
                        <div className="thread-img">
                            <img src={person} alt="" />
                        </div>
                        <div className="thread-header">
                            <span>Invest in the Success of Our Customer</span>
                            <i className="material-icons">search</i>
                        </div>
                    </div>
                    <div className="thread-reply">
                        <div className="thread-indicator">
                            <img src={arrow} alt="reply indicator" />
                        </div>
                        <div className="thread-img">
                            <img src={person2} alt="" />
                        </div>
                        <div className="thread-header">
                            <span>Nimble Releases</span>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
