import React, { Component } from "react";

export default class Form extends Component {
    state = { ...this.props.goal };
    onChange = e => {
        e.persist();
        this.setState(
            {
                [e.target.name]: e.target.value
            },
            () => {
                this.props.setGoal(this.state);
            }
        );
    };

    onDelete = id => {
        this.props.deleteGoal(this.state.id);
    };

    render() {
        return (
            <div className="form">
                <div className="d-flex justify-content-between align-items-center mb-3">
                    <div className="btn-close" onClick={this.onDelete}>
                        <i className="material-icons">close</i>
                    </div>
                    <div className="form-group m-0">
                        <label>
                            <input
                                type="text"
                                className="form-control"
                                placeholder="Type a new goal title here"
                                name="title"
                                onChange={this.onChange}
                                value={this.state.title}
                            />
                        </label>
                    </div>
                </div>
                <div className="form-group sl-4">
                    <textarea
                        className="form-control"
                        id="exampleFormControlTextarea1"
                        placeholder="Type a goal description here"
                        name="description"
                        onChange={this.onChange}
                        value={this.state.description}
                    />
                </div>
            </div>
        );
    }
}
