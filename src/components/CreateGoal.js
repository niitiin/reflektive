import React, { PureComponent } from "react";
import { connect } from "react-redux";
import { getGoalsSelector } from "../selectors";

import * as actions from "../actions";

import Form from "./Form";

class CreateGoal extends PureComponent {
    setGoal = goal => {
        this.props.updateGoal(goal);
    };

    deleteGoal = id => {
        this.props.removeGoal(id);
    };

    onSubmit = e => {
        e.preventDefault();
        this.props.saveGoals([...this.props.goals]);
    };

    addNewGoal = () => {
        this.props.newGoal({
            title: "",
            description: ""
        });
    };

    render() {
        const Goals = this.props.goalsList.length ? (
            this.props.goalsList.map((goal, id) => {
                const goalData = { ...goal, id };
                return (
                    <Form
                        goal={goalData}
                        setGoal={this.setGoal}
                        key={id}
                        deleteGoal={this.deleteGoal}
                    />
                );
            })
        ) : (
            <p>Please add a new Goal</p>
        );
        return (
            <div className="card card-body">
                <form onSubmit={this.onSubmit}>
                    {Goals}
                    <div className="d-flex justify-content-between">
                        <button
                            type="button"
                            className="btn btn-md btn-warning"
                            onClick={this.addNewGoal}
                        >
                            Add a New Goal
                        </button>
                        <button
                            type="submit"
                            className="btn btn-md btn-success"
                        >
                            Save
                        </button>
                    </div>
                </form>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        goals: getGoalsSelector(state)
    };
};

const mapDispatchToProps = dispatch => {
    return {
        newGoal: goal => actions.newGoal(goal, dispatch),
        updateGoal: goal => actions.updateGoal(goal, dispatch),
        saveGoals: () => actions.saveGoals(dispatch),
        removeGoal: id => actions.removeGoal(id, dispatch)
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(CreateGoal);
