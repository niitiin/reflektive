import axios from "axios";

const apiUrl = "http://localhost:9000/goals";

export const getGoals = () => {
    return axios.get(apiUrl);
};

export const saveGoals = () => {
    return axios.post(apiUrl);
};

export const deleteGoals = () => {
    return fetch("http://example.com/api/v1/registration", {
        method: "DELETE",
        headers: {
            "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
            "Access-Control-Allow-Origin": "*"
        }
    });
    // return axios("https://jsonplaceholder.typicode.com/posts/1", {
    //     method: "DELETE"
    // });
};
