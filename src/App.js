import React, { PureComponent } from "react";
import { connect } from "react-redux";
import { getGoalsSelector } from "./selectors";

import * as actions from "./actions";

import CreateGoal from "./components/CreateGoal";
import Design from "./components/Design";
import Navbar from "./components/Navbar";

class App extends PureComponent {
    componentWillMount() {
        this.props.getGoals();
    }

    render() {
        return (
            <div className="wrap">
                <Navbar />
                <div className="App container">
                    <div className="col-sm-6 mb-3">
                        <h4>Part 1</h4>
                        <Design />
                    </div>
                    <div className="col-sm-6">
                        <h4>Part 2</h4>
                        <CreateGoal goalsList={this.props.goals} />
                    </div>
                </div>
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        goals: getGoalsSelector(state)
    };
};

const mapDispatchToProps = dispatch => {
    return {
        getGoals: () => actions.getGoals(dispatch)
    };
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(App);
