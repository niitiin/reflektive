import { createSelector } from "reselect";

const getGoals = state => state.goals.goals;

export const getGoalsSelector = createSelector(
    [getGoals],
    goals => goals
);
